<?php
//https://webetu.iutnc.univ-lorraine.fr/www/besoin3u/crazy-charlie_day
require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;

use crazycharlieday\controllers as c;

//use Controller as TEST; 

$db = new DB();

$ini = parse_ini_file('config.ini');

$GLOBALS['prefix'] = parse_ini_file('configWeb.ini')['prefix'];
$app= new \Slim\Slim;

$db->addConnection($ini);

$db->setAsGlobal();
$db->bootEloquent();

$GLOBALS['app']=$app;

session_start();

/*
if (!isset($_SESSION['control']))
	$_SESSION['control'] = new Controller();
*/

$app->get('/', function () { (new c\Controller())->index(); })->name('INDEX');

$app->get('/listeParCategorie/:cat', function($cat){
	(new c\Controller())->listeParCategorie($cat);
})->name('listeParCategorie');

$app->get('/item/:item', function($item){
	(new c\Controller())->affichageItem($item);
})->name('affichageItem');

$app->get('/reservations/:item', function($item){
	(new c\Controller())->reservationsItem($item);
})->name('reservationsItem');

$app->get('/reserver/:item', function($item){
	(new c\Controller())->reserverItem($item);
})->name('reserverItem');


$app->run();