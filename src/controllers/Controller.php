<?php

namespace crazycharlieday\controllers;

require 'vendor/autoload.php';

use \crazycharlieday\models\Item;
use \crazycharlieday\models\Categorie;
use \crazycharlieday\models\User;
use \crazycharlieday\models\Reserve;

class Controller{


	public function index(){
		$urlCss = "https://webetu.iutnc.univ-lorraine.fr/www/besoin3u/crazy-charlie_day/src/style.css";
		$url = $GLOBALS['app']->request->getRootUri();
		$a =  <<<END
		<!doctype html>
		<html lang="fr">
		<head>
		  <meta charset="utf-8">
		  <title>Garage Solidaire</title>
		 <link rel="stylesheet" type="text/css" href="$urlCss">
		 
		</head>
		<body>
		<div class="header">
		<h1> Garage Solidaire</h1>
		</div>
		 <a href="$url/listeParCategorie/1" class="bouton">Véhicules</a>
		<a href="$url/listeParCategorie/2" class="bouton">Ateliers</a>
		</div>
		</body>
		</html>
END;



		print $a;
	}
	
	public function listeParCategorie($cat){
			$urlCss = "https://webetu.iutnc.univ-lorraine.fr/www/besoin3u/crazy-charlie_day/src/style.css";
		$categorie = Categorie::where("id","=",$cat)->get();
		$items = Item::where("id_categ","=",$cat)->get();
		$a = "
		<!doctype html>
		<html lang='fr'>
		<head>
		  <meta charset='utf-8'>
		  <title>Garage Solidaire</title>
		  <link rel='stylesheet' type='text/css' href='$urlCss'>
		</head>
		<body>
			<div class='header'>
			<h1>Liste des items:<br></h1></div>";
			$url = $GLOBALS['app']->request->getRootUri();
		foreach($items as $v){
			$idItem = $v->id;
			$a .="<form name'$idItem' method='post' action=''><a href='$url/item/$idItem'>$v->nom<br></a></form>";
		}
		$a .="</body></html>";
		echo <<<END
		$a
END;
	}
	
	
	public function affichageItem($id){
		$item = Item::where("id","=",$id)->first();
		$nom=$item->nom;
		$idItem=$item->id;
		$desc=$item->description;
		$idC=$item->id_categ;
		$categ = Categorie::where("id","=",$idC)->first();
		$nomC=$categ->nom;
		$a = "<br><hr><br>Description de l'item $idItem -  $nom<br><br>$desc<br><br><br>Categorie : $nomC<br><br>";
		$url = $GLOBALS['app']->request->getRootUri();
		$urlImage="https://webetu.iutnc.univ-lorraine.fr$url/src/images/".$item->id.".jpg";
		$a .="<img src='$urlImage' alt=\"Photo Item\">"."<br><br>";
		$a .="<a href='$url/reservations/$idItem'>Liste des reservations de l'item<br></a>";
		$a .="<a href='$url/reserver/$idItem'>Reserver l'item<br></a>";
		echo <<<END

		$a
END;
	}
	
	public function reservationsItem($id){
		$reserve = Reserve::where("id_item","=",$id)->first();
		if($reserve != null){
			$user = User::where("id","=",$reserve->id_membre)->first();
			$url = $GLOBALS['app']->request->getRootUri();
			$nom = $user->nom;
			$a ="<a href='$url'>L'item est reserve par $nom<br></a>";
		}else{
			$a = "<a href='$url'>L'item n'est pas reserve<br></a>";
		}
		echo <<<END

		$a
END;
	}
	
	
	public function reserverItem($id){
		$reserve= Reserve::where("id_item","=",$id)->first();
		$url = $GLOBALS['app']->request->getRootUri();
		if($reserve==null){
			$reserve=new Reserve();
			$reserve->id_membre=1;
			$reserve->id_item=$id;
			$reserve->save();
			$a = "<a href ='$url'> Reservation effectuee!!!</a>";
		}else{
			$a = "<a href ='$url'> Reservation impossible</a>";
		}		
	
		echo <<<END
		
		$a
END;
	}
	
		/**
		$this->main->addContent(new TitleSection("Faites votre liste de voeux !"));

		$pres = new DoubleSection();
		$pres->addContentTo(1, new Image(Resources::load("img/to-do-list.png"), "[to-do-list]"));
		$pres->addContentTo(2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae bibendum nisl, at condimentum diam. In hac habitasse platea dictumst. Nullam tempus feugiat orci et faucibus. In hac habitasse platea dictumst. Interdum et malesuada fames ac ante ipsum primis in faucibus.');
		$this->main->addContent($pres);

		$this->main->addContent(new MainLink(Resources::url("SIGNUP"), "Inscrivez-vous maintenant !"));
		*/
	
/*
	protected function generateNav() {
		
		return new NavView(array(Resources::url('SIGNIN') => "Connexion", Resources::url('SIGNUP') => "Inscription" ));
	}

	public function signup(){

		echo new SignupView(Resources::url('VALIDATESIGNUP'));
	}
	
	public function list($url){
		
		if(isset($_GET['textGuestName'])){
			$txt= filter_var($_GET['textGuestName'], FILTER_SANITIZE_STRING); 
			echo "nom:".$txt;
			$_SESSION['guestName']=$txt;
		//$this->list($_SESSION['lastUrlListe']);
		//$this->list($_SESSION['lastUrlListe']);
		}
		


			$dernieruUrl= explode("/","http:///$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
			
			$_SESSION['lastUrlListe']=$dernieruUrl[count($dernieruUrl)-1];
		
	
		$pf = new PageFactory("MyWishList");

		$list = Liste::where('url', '=', $url)->first();

		$main = new MainBox("blue");
		$pf->addContent($main);
	
		$f = new Form(Resources::url('VALIDATERESERVATION'),"post");
		
		$main->addContent(new TitleSection($list->titre));

		$main->addContent($f);
	
		
		$idliste=$list->no;
		
		foreach (WishItem::for($list->no) as $item) {

			$reserve = Item::where('id','=',$item->item->id)->first()->reserve;
			if(isset($_SESSION['guestName'])){
				echo "il y a bien un nom";
			$f->addContent($item->generate($reserve == NULL || $_SESSION['guestName'] == $reserve, false));
		}else{
				echo "il y n'ya pas un nom";
			$f->addContent($item->generate($reserve == NULL || false, false));
		}
		}

		$sect = new FullSection();
		$sect->addContent(new SubmitButton("Valider"));
		$f->addContent($sect);
		echo $pf->generate();
	}
	
		public function validateReservation(){
	
		if (!isset($_SESSION['guestName'])){
			$pf = new PageFactory("MyWishList");
			$main = new MainBox("red");
			$warnings = new FullSection();
			$form = new Form(Resources::url("LIST",array('url' => $_SESSION['lastUrlListe'])), "get");
			$main->addContent($form);
			$warnings->addContent(new TextField("textGuestName", "Veuillez saisir votre nom", ""));
			$form->addContent($warnings);
			$form->addContent(new SubmitButton("Valider"));
		//	$main->addContent($warnings);
			$pf->addContent($main);
			echo $pf->generate();	
		
		}else{
				
			$pf = new PageFactory("MyWishList");
			$main = new MainBox("red");
			echo "Session url:".$_SESSION['lastUrlListe'];
			$main->addContent(new MainLink(Resources::url("LIST" ,array('url' => $_SESSION['lastUrlListe'])),"Retour à la liste "));
			$warnings = new FullSection();
			$main->addContent($warnings);
			$pf->addContent($main);
			
			$items = array();
			$i=0;
			foreach($_POST['items'] as $item){		
				$id= filter_var($item, FILTER_SANITIZE_NUMBER_INT); 
				$items[$i]=$id;
				$i=$i+1;
			}
			
			foreach($items as $idReserve){
				$i=Item::where('id','=',$idReserve)->first();
				$i->reserve=$_SESSION['guestName'];
				$i->save();
			}	
			
			echo $pf->generate();
		}
	}

	
	
	public function signin(){

		echo new SigninView(Resources::url('VALIDATESIGNIN'));
	}

	public function validateSignup(){
		
		//filtrage des $_POST avec filter_var($var, $filter, $options)
		$pwd= filter_var($_POST['password'], FILTER_SANITIZE_STRING); 
		$pwd2= filter_var($_POST['password2'], FILTER_SANITIZE_STRING); 
		$usr= filter_var($_POST['username'], FILTER_SANITIZE_STRING); 
		$email=filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
		
		if(!$pwd || !$pwd2 || !$usr || !$email){
			echo new ValidationView(false, "Données d'inscription invalides", Resources::url("SIGNUP"), "Retour à l'inscription");
		}else{
			if($pwd!=$_POST['password']){
				echo new ValidationView(false, "Retrait des balises html du mot de passe", Resources::url("SIGNUP"), "Retour à l'inscription");
			}
			else if($usr!=$_POST['username']){
				echo new ValidationView(false, "Retrait des balises html du username", Resources::url("SIGNUP"), "Retour à l'inscription");
			}
			
			else if (strlen($pwd)>=6){
					//$m=Member::where('pseudo','=',$_POST['username'])->get();
					//if($m=="[]"){
					$m=Member::where('pseudo','=',$usr)->count();
					if($m==0){
						if($pwd == $pwd2 ){
							$hash=password_hash($pwd,PASSWORD_DEFAULT);
							$member=new Member();
							$member->email=$email;
							$member->pseudo=$usr;
							$member->mdp=$hash;
							$member->save();
							echo new ValidationView(true, "Inscription réussie", Resources::url("INDEX"), "Retour à l'index");
						}else{
							echo new ValidationView(false, "Les deux mots de passe ne correspondent pas", Resources::url("SIGNUP"), "Retour à l'inscription");
						}	
					}else{
						echo new ValidationView(false, "Un utilisateur possède déjà ce nom", Resources::url("SIGNUP"), "Retour à l'inscription");
					}
			}else {
				echo new ValidationView(false, "Le mot de passe doit contenir au moins 6 caracteres", Resources::url("SIGNUP"), "Retour à l'inscription");
			}	
		}
	}
	
	public function validateSignin(){

		$pwd= filter_var($_POST['password'], FILTER_SANITIZE_STRING); 
		$usr= filter_var($_POST['username'], FILTER_SANITIZE_STRING); 
			
		if(Member::where('pseudo','=', $usr)->count()==1){
			
			$mdp=Member::select("mdp")->where('pseudo','=', $usr)->first()->mdp;
		
			if(password_verify($pwd,$mdp) ){
		
				$_SESSION['control'] = new MemberController(Member::select("id")->where('pseudo','=', $usr)->first()->id);
				echo new ValidationView(true, "Connexion réussie", Resources::url("INDEX"), "Retour à l'index");
					
			} else {
			
				echo new ValidationView(false, "Le mot de passe ne correspond pas", Resources::url("SIGNIN"), "Retour à la connexion");
			}	
		} else {
			echo new ValidationView(false, "L'utilisateur n'existe pas", Resources::url("SIGNIN"), "Retour à la connexion");
		}
	}
	
	**/
}