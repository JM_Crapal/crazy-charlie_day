<?php

namespace crazycharlieday\models;

class Reserve extends \Illuminate\Database\Eloquent\Model {
	
		protected $table = 'reserve';
		protected $primaryKey = 'id';
		public $timestamps = false ;
}
