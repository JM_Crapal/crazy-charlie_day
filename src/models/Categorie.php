<?php

namespace crazycharlieday\models;

class Categorie extends \Illuminate\Database\Eloquent\Model {
	
		protected $table = 'categorie';
		protected $primaryKey = 'id';
		public $timestamps = false ;
		
		public function items(){
			return $this->hasMany('\crazycharlieday\models\Item','id_categ');
		}
}
