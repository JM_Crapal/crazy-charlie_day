<?php

namespace crazycharlieday\models;

class Item extends \Illuminate\Database\Eloquent\Model {
	
		protected $table = 'item';
		protected $primaryKey = 'id';
		public $timestamps = false ;
		
		public function categorie(){
			return $this->belongsTo('\crazycharlieday\models\Categorie','id_categ');
		}
}
